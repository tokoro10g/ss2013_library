//
// motor.c : 創造設計第二 H8/36064 DCモータ動作用関数
//

// ＜＜　注　意　＞＞
// Vstone社製マイコンボード（VS-WRC003）の『VS-WRC003 テクニカルマニュアル』には、
// P60端子（FTIOA0）からのPWM出力を用いて、CN1/M1コネクタよりDCモータを回転できると書いてあるが、
// 実際には、FTIOA0はPWM出力の機能を有していない。
// (「H8/36064 グループハードウェアマニュアル 『12. タイマZ　12.2 入出力端子』」参照)
// そのため、本DCモータ関連関数ライブラリでは、P60端子（FTIOA0）、およびCN1/M1コネクタを取り扱わない。


#include <machine.h>
#include "iodefine.h"
#include "motor.h"

// モータードライバにつなぐのはP63とP67
// port=0 は 入力が(PWM,IN1,IN2)のタイプのとき
// port=1 は 入力が(PWM1,PWM2)のタイプのとき

void MotorInit(char port)
{
	/* PWMのポート・回転方向の設定 */
	switch (port) {
		case 0:	// PWM：P61     回転方向：P32, P33
			//IO.PCR3 |= 0x0C;	// P32, P33を出力に設定
			//IO.PDR3.BIT.B2 = 0;	// P32の初期値を0に設定
			//IO.PDR3.BIT.B3 = 0;	// P33の初期値を0に設定
			//IO.PCR6 |= 0x02;	// P61を出力に設定
			break;
			
		case 1:	// PWM：P62 P63
			IO.PCR6 |= 0x0C;	// P62, P63を出力に設定
			break;

		default:
			break;
	}

	/* タイマZ0の設定 */
	set_imask_ccr(1);		// タイマの設定の前にはこれを書く
	TZ0.TCR.BIT.TPSC = 2;	// [010] φ/4でカウント

	switch (port) {
		case 0:	// PWM：P61     回転方向：P32, P33
			//TZ.TPMR.BIT.PWMB0 = 1;	// FTIOB0をPWM出力に設定
			//TZ.TOER.BIT.EB0 = 0;	// FTIOB0からの出力を許可
			//TZ0.GRB = 0;			// FTIOB0のデューティ0
			break;
			
		case 1:	// PWM：P62 P63
			TZ.TPMR.BIT.PWMC0 = 1;	// FTIOC0をPWM出力に設定
			TZ.TPMR.BIT.PWMD0 = 1;	// FTIOD0をPWM出力に設定
			TZ0.GRC = 0;			// FTIOC0のデューティ0
			TZ0.GRD = 0;			// FTIOD0のデューティ0
			TZ.TOER.BIT.EC0 = 0;	// FTIOC0からの出力を許可
			TZ.TOER.BIT.ED0 = 0;	// FTIOD0からの出力を許可
			break;

		default:
			break;
	}
	
	TZ.TSTR.BIT.STR0 = 1;	// タイマZ0を起動させる
	set_imask_ccr(0);		// タイマを設定し終わったらこれを書く
}



void Motor(char port, char dir, unsigned int duty)
{
	switch (port) {
		case 0:	// PWM：P61     回転方向：P32, P33
			if (dir == 0) {				/* 正転 */
				IO.PDR3.BIT.B2 = 1;		// P32出力値 = 1
				IO.PDR3.BIT.B3 = 0;		// P33出力値 = 0
			} else {					/* 逆転 */
				IO.PDR3.BIT.B2 = 0;		// P32出力値 = 0
				IO.PDR3.BIT.B3 = 1;		// P33出力値 = 1
			}

			TZ0.GRB = duty;				// GRBにコンペア値を設定
			break;
			
		case 1:	// PWM：P62 P63
			if (dir == 0) {				/* 正転 */
				TZ0.GRC = 0x0000;       // IN1(P62):0 , IN2(P63):PWM
				TZ0.GRD = duty;
			} else {					/* 逆転 */
				TZ0.GRC = duty;         // IN1(P62):PWM , IN2(P63):0
				TZ0.GRD = 0x0000;
			}
			break;

		default:
			break;
	}
}