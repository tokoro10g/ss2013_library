// アナログ入力用ライブラリ

void Analog_init();
void Analog_start();
void Analog_stop();
unsigned char Analog_read(int port);
