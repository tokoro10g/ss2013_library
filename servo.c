//
// servo.c : 創造設計第二 H8/36064
//

#include <machine.h>
#include "iodefine.h"
#include "servo.h"

void Servo_init(int angle)
{
	IO.PCR6 |= 0x40;	// P66を出力に設定

	/* タイマZ1の設定 */
	set_imask_ccr(1);		// タイマの設定の前にはこれを書く
	TZ1.TCR.BIT.TPSC = 2;	// [010] φ/4でカウント
	TZ1.TCR.BIT.CCLR = 1;
	TZ.TPMR.BIT.PWMC1 = 1;	// FTIOC1をPWM出力に設定
	TZ.TPMR.BIT.TOC1 = 1;
	TZ1.GRA = 0xFFFE;
	Servo_setAngle(angle);
	TZ.TOER.BIT.EC1 = 0;	// FTIOC1からの出力を許可

	TZ.TSTR.BIT.STR1 = 1;	// タイマZ1を起動させる
	set_imask_ccr(0);		// タイマを設定し終わったらこれを書く
}


void Servo_setAngle(int angle)
{
	TZ1.GRC=0x15E3+(unsigned int)(36.9f*(float)angle);
}