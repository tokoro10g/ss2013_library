#include "nosprintf.h"

// 数値→文字列変換関数 ========================================================

void htoa(unsigned long n, char *str)
{
	char i, c, count = 0;
	unsigned long a;
	
	// 桁数の確認
	a = n;
	while ((a >>= 4) != 0)
		count++;
	
	i = count;
	
	// 後ろから数字を入れていく
	do {
        c = n & 0x0f;
		str[i--] = c + ((c < 10) ?  '0' : 'A' - 10);
	} while ((n >>= 4) != 0);
	
	// 終端文字
	str[count + 1] = '\0';
}

void itoa(long n, char *str)
{
	char i, count = 0;
	long a;
	
	// 符号の確認
	if (n < 0) {
		n = -n;
		str[count++] = '-';
	}
	
	// 桁数の確認
	a = n;
	while ((a /= 10) > 0)
		count++;

	i = count;

	// 後ろから数字を入れていく
    do
        str[i--] = n % 10 + '0';
    while ((n /= 10) > 0);
	
	// 終端文字
    str[count + 1] = '\0';

}

void ftoa(float n, char *str, char figure)
{
	char i, count;
	long n_int = (long)n;
	
	// 整数部
	itoa(n_int, str);
	for (count = 0; str[count]!='\0'; count++);
	str[count] = '.';
	
	// 小数部
	n = n - n_int;
	if (n < 0) n = -n;
	
	for (i = 0; i < figure; i++) {
		n *= 10;
	}
	itoa(n, &str[count + 1]);
}

void htoa_cat(unsigned long n, char *str)
{
	char i;
	
	for (i = 0; str[i] != '\0'; i++);
	htoa(n, &str[i]);
}

void itoa_cat(long n, char *str)
{
	char i;
	
	for (i = 0; str[i] != '\0'; i++);
	itoa(n, &str[i]);
}

void ftoa_cat(float n, char *str, char figure)
{
	char i;
	
	for (i = 0; str[i] != '\0'; i++);
	ftoa(n, &str[i], figure);
}

// 文字列→数値変換関数 ========================================================

long atoi(char *str){
	long answer = 0;
	int s = 1;

	// スペースをスキップ
	while (*str == ' ')	str++;

	// 符号の判定
	if (*str == '-') {
		s = -1;
		str++;
	}

	// 0-9の数字を数値に変換していく
	while((*str >= '0') && (*str <= '9')){
		answer = answer * 10 + (int)(*str - '0');
		str++;
	}
	
	return answer * s;
}


