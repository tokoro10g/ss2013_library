#include <machine.h>
#include "iodefine.h"
#include "sci.h"

// シリアル通信関連関数 ========================================================

void sciinit()
{
	// 動作停止
	SCI3.SCR3.BIT.TE = 0;	// 送信動作停止
	SCI3.SCR3.BIT.RE = 0;	// 受信動作停止
	
	// シリアルコントロールレジスタ3(SCR3)の設定
	SCI3.SCR3.BIT.CKE = 0;	// [00] クロックソースは内部ボーレートジェネレータ

	// シリアルモードレジスタ(SMR)の設定
	SCI3.SMR.BIT.COM = 0;	// 調歩同期式モード
	SCI3.SMR.BIT.CHR = 0;	// データ長8ビット
	SCI3.SMR.BIT.PE = 0;	// パリティは使用しない
	SCI3.SMR.BIT.STOP = 0;	// 1ストップビット
	SCI3.SMR.BIT.MP = 0;	// マルチプロセッサモードは使用しない
	SCI3.SMR.BIT.CKS = 0;	// [00] クロックソースはφ

	// ビットレートレジスタ(BRR)の設定
	SCI3.BRR = 11;		// 38400bps
	
	// シリアルステータスレジスタ(SSR)の設定
	SCI3.SSR.BIT.OER = 0;	// オーバーランエラーをクリア
	SCI3.SSR.BIT.FER = 0;	// フレーミングエラーをクリア
	SCI3.SSR.BIT.PER = 0;	// パリティエラーをクリア
	
	// 動作開始
	SCI3.SCR3.BIT.TE = 1;	// 送信動作開始
	SCI3.SCR3.BIT.RE = 1;	// 受信動作開始
	
	// P22/TXD端子の機能の設定
	IO.PMR1.BIT.TXD = 1;	// TXD出力端子として使用
}

char scigetc()
{
     char ch;
	 
     while ((SCI3.SSR.BYTE & 0x78) == 0);	// 受信とエラーのフラグが立つまで待つ
	 
     if(SCI3.SSR.BIT.RDRF == 1){            // 正常に受信できた
          ch = SCI3.RDR;                  	// 受信したデータを格納
          return ch;
     } else {								// 受信中にエラーが発生した
          SCI3.SSR.BYTE &= 0xc7;            // エラーフラグのクリア
          return 0xff;						// 0xffを返す
     }
}

char *scigets(char *str)
{
	char *p, ch;
	
	p = str;
	
	while (1) {
		ch = scigetc();
		if (ch == '\r') break;
		*p++ = ch;
	}
	*p = '\0';
	
	return str;	
}

void sciputc(char ch)
{
     while (SCI3.SSR.BIT.TDRE == 0);		// 未送信データの送信完了まで待つ

     SCI3.TDR = ch;						// 送信するデータを格納
}

void sciputs(char *str)
{
	char *p;
	
	for (p = str; *p != '\0'; p++)
		sciputc(*p);
}
