// アナログ入力用ライブラリ
// PSD : PB4,PB5
// 磁気：PB6

// CHANNEL : 1において
// PSD1 : port 0
// PSD2 : port 1
// 磁気 : port 3

void Analog_init(){
	AD.ADCSR.BIT.SCAN = 1; // スキャンモード
	AD.ADCSR.BIT.CH = 1;
	AD.ADCSR.BIT.ADST = 1; // 割り込みはしない
	}

void Analog_start(){
	AD.ADCSR.BIT.ADST = 1;
	}

void Analog_stop(){
	AD.ADCSR.BIT.ADST = 0;
	}

unsigned char Analog_read(int port){
	switch(port){
		case 0:
			return (unsigned char)(AD.ADDRA >> 8);
			break;
		case 1:
			return (unsigned char)(AD.ADDRB >> 8);
			break;
		case 2:
			return (unsigned char)(AD.ADDRC >> 8);
			break;
		case 3:
			return (unsigned char)(AD.ADDRD >> 8);
			break;
		default:
			return 0;
		}
	}

